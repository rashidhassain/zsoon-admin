import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class adminstrationServices {
    constructor(private http: HttpClient) { }
    userid: any = localStorage.getItem('userId');

    GetAllUsers() {
         this.userid = localStorage.getItem('userId');
        return this.http.get(environment.baseURL + 'User/GetAllUsers', {
            headers: new HttpHeaders()
                .append('Authorization', 'Bearer ' + localStorage.getItem('token'))
                .append('userId', this.userid),
        });
    }

    GetAllUsersTypes() {
        this.userid = localStorage.getItem('userId');
       return this.http.get(environment.baseURL + 'User/GetAllUserTypes', {
           headers: new HttpHeaders()
               .append('Authorization', 'Bearer ' + localStorage.getItem('token'))
               .append('userId', this.userid),
       });
   }




    GetAllRoles() {
        this.userid = localStorage.getItem('userId');
        return this.http.get(environment.baseURL + 'Role/GetAllRoles', {
            headers: new HttpHeaders()
                .append('Authorization', 'Bearer ' + localStorage.getItem('token'))
                .append('userId', this.userid),
        });
    }


    addUser(datatosend) {
        this.userid = localStorage.getItem('userId');
        return this.http.post(environment.baseURL + 'User/InsertUser', datatosend,{
            headers: new HttpHeaders()
                .append('Authorization', 'Bearer ' + localStorage.getItem('token'))
                .append('userId', this.userid),
        });
    }



}
