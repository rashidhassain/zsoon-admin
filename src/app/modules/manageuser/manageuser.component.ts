import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { adminstrationServices } from 'src/zsoonServices/adminstration.service';




@Component({
  selector: 'app-manageuser',
  templateUrl: './manageuser.component.html',
  styleUrls: ['./manageuser.component.scss'],
})
export class ManageuserComponent implements OnInit {
  adduser = false;
  edituser = false;
  save = false
  userData
  usersList: any = []
  userTypes: any = []
  userDatatype
  firstName = ""
  lastName = ""
  email = ""
  mobileNo = ""
  password = ""
  userTypeCode = ""


  public addusers: any = {
    "roleId": '',
    "firstName": "",
    "lastName": "",
    "email": "",
    "mobileNo": "",
    "password": "",

    "userTypeCode": "",
    "createdBy": ""

  }


  public editusers: any = {
    "roleId": '',
    "firstName": "",
    "lastName": "",
    "email": "",
    "mobileNo": "",
    "password": "",

    "userTypeCode": "",
    "createdBy": ""

  }

  constructor(private spinner: NgxSpinnerService, private adminstrationService: adminstrationServices) { }

  ngOnInit(): void {
    // this.GetAllUsers()
    this.spinner.show();
    // this.GetAllUsersTypes()

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 550);
  }
  applyFilter(event: Event) { }


  // api's start
  // GetAllUsers() {
  //   this.adminstrationService.GetAllUsers().
  //     subscribe(data => {

  //       this.userData = data
  //       this.usersList = this.userData.users
  //       console.log(data, this.userData, this.userData.users, this.usersList)

  //     })
  // }


  // GetAllUsersTypes(){
  //   this.adminstrationService.GetAllUsersTypes().subscribe(data=> {
  //     this.userDatatype = data
  //     this.userTypes = this.userDatatype.userTypes 
  //     console.log(data,this.userDatatype,this.userDatatype.userTypes,this.userTypes)

  //   })
  // }


  // api's end

  AddUser() {
    this.adduser = true;
    this.edituser = false;
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 500);
  }

  EditUser(user) {
    this.edituser = true;
    this.adduser = false;
this.editusers = user
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 500);
  }

  Cancel() {
    this.adduser = false;
    this.edituser = false;
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 500);
  }


  userType(id) {

    console.log("passing",id)
  }



  SubmitUsers(data) {
    console.log(this.firstName)
    console.log("addusers", data)
    const datatosend = {
      "roleId": data.roleId,
      "firstName": data.firstName,
      "lastName": data.lastName,
      "email": data.email,
      "mobileNo": data.mobileNo,
      "password": data.password,

      "userTypeCode": data.userTypeCode,
      "createdBy": data.createdBy
    }
   // console.log("addinguser", datatosend)

    // this.adminstrationService.addUser(datatosend).subscribe(data=>{
    //   console.log("useraddedsuccess",data)
    //   this.GetAllUsers()
    // })
  }





}
