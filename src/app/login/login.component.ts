import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { loginServices } from '../../zsoonServices/loginservices';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  alert = false;
  alert2 = false;
  returnUrl = '/dashboard';
  public logindata = {
    email: '',
    password: '',
  };

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private loginservices: loginServices
  ) {}

  ngOnInit(): void {


    // spinner
    this.spinner.show();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1500);
  }
  submit(data) {
    if (data.email !== '' && data.password !== '') {
      const datasending = {
        email: data.email,
        password: data.password,
      };
      console.log('objdata', datasending);
      this.router.navigate([this.returnUrl]);
    } else {
      console.log('no');
      if (this.logindata.email == '') {
        this.alert = true;
      } else {
        this.alert = false;
      }
      if (this.logindata.password == '') {
        this.alert2 = true;
      } else {
        this.alert2 = false;
      }
    }
    console.log('passing', data);

    // this.loginservices.authenticateUser(datasending)
    // .subscribe(result=>{
    //   console.log('response',result);
    //   if(result.success){

    //     localStorage.setItem('token',result.token);
    //     localStorage.setItem('userId',result.userGuid);
    //     localStorage.setItem('roleId',result.roleId);
    //     this.router.navigate([this.returnUrl]);
    //   }

    // })

    // alert("Welcome to zsoon page")
  }
  eventHandler(data: any) {
    console.log(data);
    if(this.logindata.email!=""){
      this.alert=false
        }
       else{
         this.alert=true
       }


  }
  eventHandlers(data: any) {
    console.log(data);
    if(this.logindata.password!=""){
      this.alert2=false
        }
       else{
         this.alert2=true
       }


  }
}
